/// Renvoie un jolie message d'erreur
pub fn message_erreur(message: &str) -> String {
    format!(
        "\n{sep}\n{msg}\n{sep}\n",
        sep = "=".repeat(message.len()),
        msg = message
    )
}

/// Affiche un jolie message d'erreur
#[allow(dead_code)] // mauvaise détection alors on retire se warning
pub fn affiche_message_erreur(message: &str) {
    println!("{}", message_erreur(message))
}
