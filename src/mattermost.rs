use chrono::prelude::*;
use mattermost_api::prelude::*;
use serde::{Deserialize, Serialize};
use serde_json::json;

mod erreur {
    include!("erreur.rs");
}

/// Permet de stocker les informations nécessaire pour utiliser le websocket
pub struct ConnectionInfoWebsocket {
    /// API du client Mattermost
    pub api: mattermost_api::client::Mattermost,

    /// Token utilisé
    pub token: String,
    /* /// Prefix utilisé
    pub prefix: String */
}

/// Se connecte à Mattermost
pub async fn connexion(
    token: &str,
    pseudo: &str,
    mot_de_passe: &str,
    url: &str,
) -> mattermost_api::client::Mattermost {
    let auth;
    if !token.is_empty() {
        auth = AuthenticationData::from_access_token(token);
        // println!("Login via token");
    } else {
        auth = AuthenticationData::from_password(pseudo, mot_de_passe);
        // println!("Login via pseudo/mot-de-passe");
    }
    let mut api = Mattermost::new(url, auth);
    api.store_session_token().await.unwrap();

    api
}

/// Permet d'afficher un contenu proprement
trait Affichage {
    /// Renvoie un affichage "propre" concernant l'équipe
    fn fprint(self) -> String;
}

impl Affichage for mattermost_api::models::TeamInformation {
    fn fprint(self) -> String {
        let mut res: String = String::from("");
        res += &format!("ID => {}", self.id);

        res += &format!("\nType => {}", self.type_);

        res += &format!("\nNom => {}", self.name);

        res += &format!("\nNom d'affichage => {}", self.display_name);

        if !self.description.is_empty() {
            res += &format!("\nDescription => {}", self.description);
        }

        // La date fournis par Mattermost est incohérente ? ex: 15/08/53977 le 03/01/2022
        let format_date = "le %d/%m/%Y à %T (UTC)";

        let creation: DateTime<Utc> =
            DateTime::from_utc(NaiveDateTime::from_timestamp(self.create_at, 0), Utc);
        res += &format!("\nDate de création => {}", creation.format(format_date));

        let mise_a_jour: DateTime<Utc> =
            DateTime::from_utc(NaiveDateTime::from_timestamp(self.update_at, 0), Utc);
        res += &format!(
            "\nDernière mise-à-jour => {}",
            mise_a_jour.format(format_date)
        );

        if self.delete_at != 0 {
            let suppression: DateTime<Utc> =
                DateTime::from_utc(NaiveDateTime::from_timestamp(self.delete_at, 0), Utc);
            res += &format!(
                "\nDate de suppression => {}",
                suppression.format(format_date)
            );
        }

        res += &format!("\nEmail => {}", self.email);

        let invitation;
        if self.allow_open_invite {
            invitation = "Vrai";
        } else {
            invitation = "Faux";
        }
        res += &format!("\nInvitation ouverte à tous => {}", invitation);

        if !self.allowed_domains.is_empty() {
            res += &format!("\nNom de domaines autorisés => {}", self.allowed_domains);
        }

        res += &format!("\nID d'invitation => {}", self.invite_id);

        if let Some(id) = self.policy_id {
            res += &format!("\nID de la politique d'utilisation => {}", id);
        }

        res
    }
}

/// Response struct from /channels
///
/// Pour savoir ce qu'il faut mettre ici, il faut check la documentation
/// https://api.mattermost.com/#operation/GetAllChannels
/// et bien faire attention aux valeurs qui peuvent être null
/// -> on peut le savoir en regardant le détail de ce qui est envoyé à Mattermost
/// ex ici, il faut cliquer sur : 200 Channel list retrieval successful
/// pour accéder à la liste déroulante :)
#[derive(Debug, Deserialize, Serialize)]
pub struct ChannelInformation {
    pub id: String,
    pub create_at: i64,
    pub update_at: i64,
    pub delete_at: i64,
    pub team_id: String,
    #[serde(rename = "type")]
    pub type_: String,
    pub display_name: String,
    pub name: String,
    pub header: String,
    pub purpose: String,
    pub last_post_at: i64,
    pub total_msg_count: i64,
    pub extra_update_at: i64,
    pub creator_id: String,
    pub team_display_name: String,
    pub team_name: String,
    pub team_update_at: i64,
    pub policy_id: Option<String>,
}

/// Récupère la liste de tous les salons disponibles
pub async fn get_channel_info(
    api: &mattermost_api::client::Mattermost,
) -> Result<Vec<ChannelInformation>, ApiError> {
    api.query("GET", "channels", None, None).await
}

/// Récupère l'ID du salon renseigné grâce à son nom en faisant une requête à l'API de Mattermost
#[allow(dead_code)] // je comprends pas pourquoi j'ai besoin de mettre ça, parce que j'utilise le code en dessous...
pub async fn channel_id_by_name(api: &mattermost_api::client::Mattermost, salon: String) -> String {
    println!("\nRécupération de l'ID du salon {}...", salon);
    match get_channel_info(api).await {
        Ok(channels) => {
            for channel in &channels {
                if channel.name == salon {
                    return channel.id.clone();
                }
            }

            panic!(
                "{}",
                erreur::message_erreur("Impossible de récupérer l'ID du salon renseigné: Pas présent dans la liste récupérée")
            )
        }
        Err(err) => {
            panic!(
                "{}",
                erreur::message_erreur(&format!(
                    "Impossible de récupérer l'ID du salon renseigné: {}",
                    err
                ))
            )
        }
    }
}

/// Affiche les informations complète sur une équipe
#[allow(dead_code)] // je comprends pas pourquoi j'ai besoin de mettre ça, parce que j'utilise le code en dessous...
pub async fn team_info(api: &mattermost_api::client::Mattermost, equipe: &str) {
    println!("\nTentative de récupération de l'équipe {}...", equipe);
    match api.get_team_info(equipe).await {
        Ok(infos) => {
            println!("{}", infos.fprint());
        }
        Err(e) => {
            erreur::affiche_message_erreur(&format!("[Affichage info équipe] Erreur: {}", e));
        }
    }
}

/// Se connecte au websocket
pub async fn connect_websocket(
    api: &mattermost_api::client::Mattermost,
    token: &str,
) -> Result<String, ApiError> {
    api.query::<String>(
        "GET",
        "websocket",
        None,
        Some(
            &json!( // d'après la documentation : https://api.mattermost.com/#tag/WebSocket
                {
                    "seq": 1,
                    "action": "authentication_challenge",
                    "data": {
                        "token": &token // api.auth_token est privé
                    }
                }
            )
            .to_string(),
        ),
    )
    .await
}

/// Lance le bot Mattermost
#[allow(dead_code)] // je comprends pas pourquoi j'ai besoin de mettre ça, parce que j'utilise le code en dessous...
pub async fn start_mattermost(connection: ConnectionInfoWebsocket) {
    match connect_websocket(&connection.api, &connection.token).await {
        Ok(res) => {
            /* Pour l'instant ici on a reçois un string donc on l'affiche (Result<String, ApiError>) mais
             * à l'avenir normalement ici on bloque le thread principale (ou on laisse
             * le bot discord le bloqué et ici ça s'éxécute en parallèle au bot discord)
             * et on récupère toutes les informations dont on a besoin, c'est à dire les
             * messages envoyés. */
            println!("{}", res);
        }
        Err(e) => {
            erreur::affiche_message_erreur(&format!("[Websocket] Erreur: {}", e));
        }
    }
}

/* TODO
    !! Réflexion 1 : Websocket https://api.mattermost.com/#tag/WebSocket
    Malheureusement, aucune réponse sur une possible date de sortie du support du websocket
    dans la librarie que j'utilises (mon issue : https://github.com/Celeo/mattermost_api/issues/1)
    Cependant je pense que c'est quand même prévu à l'avenir : https://github.com/Celeo/mattermost_api/blob/master/src/client.rs#L185

    En attendant, utiliser un websocket rend tout plus simple, on récupère tout les événements qu'on reçoit, on traite les messages
    et si un utilisateur demande une commande on envoie un message en réponse (en théorie).

    -> Appel de la connexion au websocket donne une erreur HTTP 400 malheureusement

    -------------

    !! Réflexion 2 : pas de websocket, on utilise des commandes

    - Faire une fonction qui récupère la liste des commandes
        -> https://api.mattermost.com/#operation/ListCommands
    - Avec ça, on vérifie nos commandes
        -> S'il en manque, on la rajoute
            -> https://api.mattermost.com/#operation/CreateCommand
        -> S'il y en a une qui n'est plus présente, on la retire
            -> https://api.mattermost.com/#operation/DeleteCommand

    - Les commandes font une requête, doit-on faire tourner un serveur web en local ?
*/
