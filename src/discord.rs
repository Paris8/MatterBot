use serenity::{
    async_trait,
    model::{channel::Message, gateway::Ready},
    prelude::*,
};

use serde_json::json;

mod erreur {
    include!("erreur.rs");
}

mod mattermost {
    include!("mattermost.rs");
}

/* J'ai séparer les deux structures publiques pour que ce soit
 * plus clair et lisible, ensuite je réunies toutes les informations
 * dans une structure privé du fichier */

/// Identifiants pour Mattermost, récupérer une fois dans `main.rs`
pub struct IdentifiantsMattermost {
    pub token: String,
    pub user: String,
    pub pass: String,
    pub url: String,
}

/// Permet de stocker les informations nécessaire pour utiliser le bot discord
pub struct ConnectionInfoDiscord {
    pub token: String,
    pub prefix: String,
    pub salon: String,
}

/// Structure qui stocke les informations dont le bot a besoin pour communiquer avec Mattermost
struct InformationsBot {
    token: String,
    user: String,
    pass: String,
    url: String,
    prefix: Option<String>,
    salon: Option<String>,
}

/// Implémentation pour utiliser facilement les valeurs de la structure
/// Normalement, rien ne panique car si `start_discord`
/// n'as pas encore été appelé, on panique avant d'arriver ici.
///
/// On fait quand même le match pour être sûr.
impl InformationsBot {
    fn err() -> String {
        String::from("Erreur lors de la récupération des informations\nTips: start_discord n'a peut-être pas été appelée...")
    }

    /// Créer une structure
    pub fn nouveau(
        identifiants: IdentifiantsMattermost,
        prefix: String,
        salon: String,
    ) -> Option<Self> {
        Some(Self {
            token: identifiants.token,
            user: identifiants.user,
            pass: identifiants.pass,
            url: identifiants.url,
            prefix: Some(prefix),
            salon: Some(salon),
        })
    }

    /// Récupère préfix, sinon panique
    pub fn recuperation_prefix(&self) -> String {
        match &self.prefix {
            Some(prefix) => prefix.clone(),
            None => panic!(
                "{}",
                erreur::message_erreur(&format!("[Recup-Prefix] {}", Self::err()))
            ),
        }
    }

    /// Récupère API, sinon panique
    ///
    /// Question importante, je créer l'API dans `main.rs`, alors pourquoi je
    /// n'ai pas utilisé cet api pour ce fichier ? Tout simplement parce que
    /// `mattermost_api::client::Mattermost` n'implémente pas le trait de Clone
    /// impossible donc (pas de succès avec Mutex)...
    pub async fn recuperation_api(&self) -> mattermost_api::client::Mattermost {
        mattermost::connexion(&self.token, &self.user, &self.pass, &self.url).await
    }

    /// Récupère le salon, sinon panique
    pub fn recuperation_salon(&self) -> String {
        match &self.salon {
            Some(salon) => salon.clone(),
            None => panic!(
                "{}",
                erreur::message_erreur(&format!("[Recup-Salon] {}", Self::err()))
            ),
        }
    }
}

static mut _INFO: Option<InformationsBot> = None;

/// Récupère les informations qui sont nécessaire à la communication avec Mattermost
/// Attention, on fait paniqué le programme si la valeur est vide, pour éviter
/// ça, il faut appeler cette fonction seulement quand `start_discord`
/// a été appelé car c'est elle qui ajoute le préfixe à `_INFO`
///
/// Je sais que c'est vraiment naze de faire ça (variable static + blocs unsafe)
/// mais c'est la seul solution que j'ai trouvé en évitant de trop étoffé avec du
/// code Discord pas très utile.
unsafe fn recuperation_info() -> &'static InformationsBot {
    match &_INFO {
        Some(info) => info,
        None => panic!(
            "{}",
            erreur::message_erreur(&format!("[Recup-InfosBot] {}", InformationsBot::err()))
        ),
    }
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    //  Appellé quand un message est récupérer par le bot
    async fn message(&self, ctx: Context, msg: Message) {
        let infos: &InformationsBot;
        unsafe {
            infos = recuperation_info();
        }
        let prefix = infos.recuperation_prefix();
        if msg.content == format!("{}info", prefix) {
            let reponse = "\
                Bot réalisé dans le cadre du cours de Programmation avancée.\n\
                Code source du bot : https://code.up8.edu/Anri/matter_bot\n\
                But du bot : Faire un pont entre Mattermost et Discord.";

            if let Err(err) = msg.reply(&ctx.http, reponse).await {
                // Gestion erreur lors de l'envoie de la réponse
                erreur::affiche_message_erreur(&format!("Erreur d'envoie de message : {:?}", err));
            }
        // Ici on peut rajouter des `if` pour ajouter des commandes...
        } else {
            /* Dans ce cas là, ce n'est pas une commande, alors c'est un message.
             * Il faut l'envoyer à Mattermost. */
            envoie_msg_mattermost(msg).await
        }
    }

    // Fonction appelé quand le bot est lancé et connecté
    async fn ready(&self, _: Context, _: Ready) {
        println!("Écoute les événements Discord...");
    }
}

/// Lance le bot Discord
pub async fn start_discord(
    informations: ConnectionInfoDiscord,
    identifiants: IdentifiantsMattermost,
) {
    unsafe {
        // On enregistre tout de suite nos informations
        _INFO = InformationsBot::nouveau(identifiants, informations.prefix, informations.salon);
    }

    // Client Discord (https://docs.rs/serenity/latest/serenity/client/index.html)
    let mut client = Client::builder(&informations.token)
        .event_handler(Handler)
        .await
        .unwrap_or_else(|_| {
            panic!(
                "{}",
                erreur::message_erreur("Impossible de créer le client Discord",)
            )
        });

    // Lance le bot
    if let Err(err) = client.start().await {
        // Si une erreur apparaît
        erreur::affiche_message_erreur(&format!("Erreur lancement discord : {:?}", err));
    }
}

/// Envoie un message sur Mattermost
async fn envoie_msg_mattermost(msg: Message) {
    let infos: &InformationsBot;
    unsafe {
        infos = recuperation_info();
    }
    let salon = infos.recuperation_salon();
    println!("Envoie d'un message de {}!", msg.author);
    let _ = infos
        .recuperation_api()
        .await
        .query::<String>(
            "POST",
            "posts",
            None,
            Some(
                &json!( // d'après la documentation : https://api.mattermost.com/#tag/WebSocket
                    {
                        "channel_id": salon,
                        "message": msg.content
                    }
                )
                .to_string(),
            ),
        )
        .await;
}
