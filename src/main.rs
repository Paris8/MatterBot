use dotenv::dotenv;
use std::env;

mod erreur;

mod discord;
mod mattermost;

#[tokio::main]
async fn main() {
    dotenv().unwrap_or_else(|_| {
        // Vérification si fichier .env trouvé
        panic!(
            "{}",
            erreur::message_erreur("Le fichier .env n'a pas été trouvé ou invalide...")
        )
    });

    // Initialisation des variables token, pseudo et mot de passe
    let mut token_mattermost = String::from("");
    let mut pseudo = String::from("");
    let mut mot_de_passe = String::from("");

    // Récupération du token mattermost
    let mut token_recuperer = false;
    if let Ok(string) = env::var("TOKEN_MATTERMOST") {
        token_mattermost = string;
        token_recuperer = true;
    }

    // Récupération du token discord
    let token_discord = env::var("TOKEN_DISCORD").unwrap_or_else(|_| {
        panic!(
            "{}",
            erreur::message_erreur("Token discord non trouvé dans le fichier .env")
        )
    });

    // Récupération du prefix
    let prefix = env::var("PREFIX").unwrap_or_else(|_| String::from("ç")); // récupération du prefix

    // Récupération du pseudo
    let mut pseudo_ok = false;
    match env::var("PSEUDO_MATTERMOST") {
        Ok(string) => {
            if token_recuperer {
                panic!("{}", erreur::message_erreur("Tu as déjà choisie un token pour Mattermost, vous ne pouvez pas aussi avoir définit un pseudo."));
            }
            pseudo_ok = true;
            pseudo = string;
        }
        Err(_) => {
            if !token_recuperer {
                panic!(
                    "{}",
                    erreur::message_erreur(
                        "Aucun token ni pseudo trouvé dans le fichier .env pour Mattermost"
                    )
                );
            }
        }
    }

    // Récupération du mot de passe
    match env::var("MDP_MATTERMOST") {
        Ok(string) => {
            if token_recuperer {
                panic!("{}", erreur::message_erreur("Tu as déjà choisie un token pour Mattermost, vous ne pouvez pas aussi avoir définit un mot de passe."));
            }
            mot_de_passe = string;
        }
        Err(_) => {
            if pseudo_ok {
                panic!(
                    "{}",
                    erreur::message_erreur(
                        "Tu as renseigner un pseudo pour Mattermost mais pas de mot de passe dans le fichier .env"
                    )
                );
            }
        }
    }
    let mut url = env::var("URL_MATTERMOST").unwrap_or_else(|_| {
        panic!(
            "{}",
            erreur::message_erreur("URL de l'instance Mattermost non trouvé dans le fichier .env")
        )
    }); // récupération de l'url
    if url.ends_with('/') {
        // Si l'url finit par un '/'
        url = url[0..url.len() - 1].to_string(); // on retire le /
    }
    if !url.starts_with("http://") && !url.starts_with("https://") {
        // Si l'url commence pas par 'http(s)'
        url = format!("http://{}", url); // on ajoute http
    }

    // Récupération équipe
    if let Ok(string) = env::var("EQUIPE_MATTERMOST") {
        // On affiche les infos d'une team
        mattermost::team_info(
            &mattermost::connexion(&token_mattermost, &pseudo, &mot_de_passe, &url).await,
            &string,
        )
        .await;
    }

    let salon = env::var("SALON_MATTERMOST").unwrap_or_else(|_| {
        // Récupération du salon
        panic!(
            "{}",
            erreur::message_erreur("Salon non trouvé dans le fichier .env")
        )
    });

    /* Désactiver car websocket marche pas (erreur HTTP 400)
    // Tâche en parallèle
    mattermost::start_mattermost(mattermost::ConnectionInfoWebsocket {
        api: mattermost::connexion(&token_mattermost, &pseudo, &mot_de_passe, &url).await,
        token: token_mattermost.clone(),
    })
    .await; */

    // Bloque le thread principale en quelque sorte
    discord::start_discord(
        /* On instancie notre structure avec les identifiants Discord
         * pour pouvoir s'y connecter, avec un salon Mattermost où les
         * messages seront envoyés. */
        discord::ConnectionInfoDiscord {
            token: token_discord,
            prefix,
            salon: mattermost::channel_id_by_name(
                &mattermost::connexion(&token_mattermost, &pseudo, &mot_de_passe, &url).await,
                salon,
            )
            .await, // récupération ID du salon
        },
        /* On instancie notre structure avec les identifiants Mattermost
         * que notre fichier Discord à besoin pour envoyer des message
         * sur Mattermost. */
        discord::IdentifiantsMattermost {
            token: token_mattermost,
            user: pseudo,
            pass: mot_de_passe,
            url,
        },
    )
    .await;
}
