# matter_bot
Au début, `matter_bot` devait être un bot qui fait le pont entre Discord et Mattermost.
Malheureusement, je n'ai pas réussi à recevoir les messages depuis Mattermost...
MAIS, tout ce qui est du côté de Discord devrait fonctionner proprement.

J'ai laissé tout ce que j'ai commencé pour Mattermost, il faut donc quand-même renseigner
les variables demandés ci-dessous pour pouvoir lancer le bot.

## Pré-requis
`openssl` requis par [`mattermost_api`](https://github.com/Celeo/mattermost_api)
```bash
sudo apt install libssl-dev
```

`.env` placé dans la racine du projet qui contient ses différentes variables :
```
TOKEN_DISCORD=votre_token_ici       # OBLIGATOIRE  | Token Discord

TOKEN_MATTERMOST=votre_token_ici    # OBLIGATOIRE? | Pas besoin de token si vous mettez un combo pseudo/mot-de-passe
PSEUDO_MATTERMOST=votre_pseudo_ici             # OBLIGATOIRE? | Pas besoin de pseudo si un token est renseigné
MDP_MATTERMOST=votre_mot-de-passe_ici # OBLIGATOIRE? | Pas besoin de mot de passe si un token est renseigné
                                    # -> Si les 2 méthode de connexion pour Mattermost sont renseignées (token et pseudo/mot-de-passe), le bot ne se lancera pas.
URL_MATTERMOST=url_instance_mattermost_ici     # OBLIGATOIRE  | Ex: https://talk.up8.edu
SALON_MATTERMOST=nom_du_salon                  # OBLIGATOIRE  | Ex: off-topic
EQUIPE_MATTERMOST=nom_de_lequipe               # FACULTATIF   | Ex: etudiant-es
                                    # -> Va permettre d'afficher les informations d'une équipe à chaque démarrage du bot.

PREFIX=votre_prefix_ici             # OBLIGATOIRE  | Commun aux deux bots, par défaut: `ç`
```

---
### Intégrations
#### Mattermost
Dans le cas ou vous optez pour le token, vous devez ajouter un bot, pour ce faire (d'après la [documentation officielle](https://developers.mattermost.com/integrate/admin-guide/admin-bot-accounts/)) :
- Cliquez sur les 9 carrés en haut à gauche
- Integrations
- Bot Accounts
- Add Bot Account
- Remplissez les champs
- Cochez la case `post:all` pour être sur qu'il n'y aura pas de problèmes de permission (facultatif)
- Create Bot Account
- Dans la page suivante, vous aurez votre token d'accès

#### Discord
Pour obtenir un token pour le bot Discord, il y a ce [tuto fait pour la librairie Javascript](https://discordjs.guide/preparations/setting-up-a-bot-application.html#creating-your-bot) qui explique bien.

---
### Tests en local
En local, j'ai utilisé Docker pour héberger mon instance de Mattermost et pouvoir faire des tests, voici le docker-compose que j'ai utilisé :
```docker
version: "2"
services:
  mattermost:
    image: mattermost/mattermost-preview:latest # version preview parce que c'est juste pour testé
    container_name: Mattermost
    ports:
      - 8065:8065 # cf. documentation officielle : https://docs.mattermost.com/install/setting-up-local-machine-using-docker.html
    restart: unless-stopped
```

---
### Sources
- [Documentation `mattermost_api`](https://docs.rs/mattermost_api/0.1.0/mattermost_api/) (utilisé pour communiquer avec Mattermost)
- `tokio` (utilisé car `mattermost_api` est `async`)
- `serde` (utilisé pour ma structure de salon [j'ai fait comme `mattermost_api`])
- `serde_json` (utilisé pour gérer le format `json`)
- [Documentation `chrono`](https://rust-lang-nursery.github.io/rust-cookbook/datetime/parse.html) (utilisé pour l'analyse des timestamps)
- [Documentation `serenity`](https://docs.rs/serenity/latest/serenity/client/trait.EventHandler.html) (utilisé pour communiquer avec Discord)
- Je me suis aidé de mes [réponses aux rustlings](https://code.up8.edu/Anri/2021-rustlings/-/tree/reponses/exercises) pour me rafraîchir la mémoire sur certains points
- [Documentation de l'`API Mattermost`](https://api.mattermost.com/) (évidemment pour savoir comment Mattermost fonctionne)

---
### Estimation du temps de build
Sur ma machine (WSL), le temps de build est d'environ une minute.

- `time cargo build --release` :
```
________________________________________________________
Executed in   68.24 secs    fish           external
   usr time  484.04 secs  128.00 micros  484.04 secs
   sys time   22.84 secs  202.00 micros   22.84 secs
```

- `cargo bench`
```
Finished bench [optimized] target(s) in 1m 03s
```

---
### Résumé
Beaucoup de blabla, au final, qu'est ce qui fonctionne ?

- [x] Pont Discord    -> Mattermost
- [ ] Pont Mattermost -> Discord
- [x] Commandes Discord
- [ ] Commandes Mattermost

Cependant, je pense avoir fait 50% du travail pour le pont Mattermost, il manque, je pense, une implémentation du websocket de la part
du [responsable de `mattermost_api`](https://github.com/Celeo). J'ai essayé de le faire moi-même mais pas réussi.

---
### Exécution

```
git clone git@code.up8.edu:Anri/matter_bot.git matter_bot_akennel
cd matter_bot_akennel
# Créez et complétez le .env
cargo run
```

<div align="center">
  <img src="assets/demonstration.gif" alt="Exemple d'utilisation de matter_bot" width="1000" />
</div>

---
*Projet ne fonctionne pas avec l'instance de la Fac, car son Mattermost se connecte via Gitlab et (je crois?) aucune librairie ne supporte ceci en Rust.*
